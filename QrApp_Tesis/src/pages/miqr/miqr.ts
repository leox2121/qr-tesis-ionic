import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { AuthProvider } from '../../providers/auth/auth';
/**
 * Generated class for the MiqrPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-miqr',
  templateUrl: 'miqr.html',
})
export class MiqrPage {

  cedula: string;
  usuario: any = {};

  constructor(private _auth: AuthProvider, public navCtrl: NavController, public navParams: NavParams, private _user: UsuarioProvider) {
    this._user.cargarUsuario().then((res: any) => {
      this.usuario = res;
      this.cedula = res.cedula;
    }).catch((err) => {
      console.log(err);
      this.usuario = {};
      this.cedula = '';
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MiqrPage');
  }

}
