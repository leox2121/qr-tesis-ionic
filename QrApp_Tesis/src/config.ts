export class config {
    public titulo_app_uno = "MENU";
    public empresa = "MENU";
    public anio = "2019";
    public version = "1.0.0";
    public UrlRoot = 'https://tesis-qr.herokuapp.com/';
    // public UrlRoot = 'http://localhost:9000/Tesis_no/public/';
    // public UrlRoot = 'http://192.168.0.104:8000/';
    public iconos = 'Imagenes/iconos/';
    public imagenes = 'Imagenes/img/';
}