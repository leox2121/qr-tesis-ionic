import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Storage } from "@ionic/storage";
/*
  Generated class for the UsuarioProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsuarioProvider {

  // usuario: any = {};

  constructor(public http: HttpClient,
    private platform: Platform,
    private storage: Storage) {
    console.log('Hello UsuarioProvider Provider');
    this.cargarUsuario();
  }

  cargarUsuario() {
    // debugger
    let usuario: any = {};
    return new Promise((resolve, reject) => {
      if (this.platform.is("cordova")) {
        this.storage.ready().then(() => {
          this.storage.get("usuario").then(usuario => {
            if (usuario) {
              usuario = JSON.parse(usuario);
            }
            resolve(usuario);
          });
        });
      } else {
        usuario = JSON.parse(localStorage.getItem("usuario"));
        resolve(usuario);
      }
    });
  }

}
