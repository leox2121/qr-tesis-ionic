import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { UtilProvider } from '../../providers/util/util';
import { RegisterPage } from '../index.paginas';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  formulario: FormGroup;
  loader;
  cmbRoles: any[] = [];
  mostrar: boolean = false;

  constructor(public fb: FormBuilder, public navCtrl: NavController, public navParams: NavParams,
    private loadingCtrl: LoadingController, private _util: UtilProvider,
    private _auth: AuthProvider
  ) {
    this.formulario = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      rol: ['', [Validators.required]],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  ingresar() {
    let loader = this.loadingCtrl.create({
      content: "Espere por favor...",
    });
    this.loader = loader;
    this._auth.IniciarSesion(this.formulario.value, this);
  }

  registar(){
    this.navCtrl.push(RegisterPage);
  }

  getRol() {
    this.mostrar = false;
    this.formulario.get('rol').setValue('');
    let data = { email: this.formulario.get('email').value };
    this._auth.getRolUsuario(data).subscribe((res: any) => {
      let resp = res.json();
      this.cmbRoles = resp.respuesta;
      if(this.cmbRoles.length < 1){
        this._util.confirmarMensaje('Error','No se encontraron roles para este usuario');
      } else {
        if(this.cmbRoles.length == 1){
          this.formulario.get('rol').setValue(this.cmbRoles[0].codigo);
        } else {
          this.mostrar = true;
        }
      }
    }, (err) => {
      console.log(err);
      this.mostrar = false;
    });
  }



}
