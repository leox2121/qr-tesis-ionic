import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

// import { ScanData } from "../../models/scan-data.model";
import { HistorialService } from "../../providers/historial/historial"
import { AuthProvider } from '../../providers/auth/auth';
import { ComunicacionProvider } from '../../providers/comunicacion/comunicacion';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilProvider } from '../../providers/util/util';

/**
 * Generated class for the GuardadosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-guardados',
  templateUrl: 'guardados.html',
})
export class GuardadosPage {
  usuario: any = {};
  formulario: FormGroup;
  items = [];
  respuesta: any[] = [];
  contador: number = 0;
  objList: any[] = [];

  constructor(
    private _util: UtilProvider, 
    private _auth: AuthProvider, 
    private _com: ComunicacionProvider, 
    public fb: FormBuilder,
    private _user: UsuarioProvider) {
      this.formulario = this.fb.group({
        id_usuario: ['', [Validators.required]],
        start_date: [''],
        end_date: ['']
      });
    this._user.cargarUsuario().then((res: any) => {
      this.usuario = res;
      this.formulario.get('id_usuario').setValue(this.usuario.id);
      this.cargarHistorial();
    }).catch((err) => {
      console.log(err);
      this.usuario = {};
    });
  }

  cargarHistorial(){
    this._com.getConsultaIngresoInfinity(this.formulario.value).subscribe((res:any)=>{
      this.respuesta = res.respuesta;
      if(this.respuesta.length < 5){
        this.items = this.respuesta;
         return;
      }
      this.objList = [];
      let lista = [];
      for (let i = 0; i < this.respuesta.length; i++) {
        const e = this.respuesta[i];
        lista.push(e);
        if(lista.length === 5 ) {
          this.objList.push(lista);
          lista = [];
        }
      }
      if(this.objList.length > 0){
        this.items = this.objList[0];
        this.contador = 1;
      }
      console.log(this.objList);
    },(e)=>{
      console.log(e.error);
      let msg = '';
      let status = '';
      if (e.status === 0) {
        msg = 'No hay conexión a internet';
      } else {
        msg = e.error.message;
        status = e.error.status;
      }
      this._util.confirmarMensaje(status, msg);
    });
  }

  doRefresh(refresher) {
    this.cargarHistorial();
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  clearDate() {
    this.formulario.get('start_date').setValue('');
    this.formulario.get('end_date').setValue('');
  }

  doInfinite(infiniteScroll) {
    if(this.respuesta.length < 5){
      infiniteScroll.complete();
      return;
    }
    let fin = this.objList.length;
    if(this.respuesta.length > 0){
        if(this.contador >= fin){
          infiniteScroll.complete();
          return;
        }
        this.contador++;;
        setTimeout(()=>{
          let e = this.objList[this.contador];
          this.items.push(...e);
          infiniteScroll.complete();
        },1000);
    }
    // console.log('Begin async operation');
    // setTimeout(() => {
    //   for (let i = 0; i < 30; i++) {
    //     this.items.push( this.items.length );
    //   }
    //   console.log('Async operation has ended');
    //   infiniteScroll.complete();
    // }, 500);
  }

}
