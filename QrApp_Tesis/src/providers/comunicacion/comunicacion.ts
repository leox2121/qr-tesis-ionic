import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UtilProvider } from '../util/util';
import { config } from '../../config';

/*
  Generated class for the ComunicacionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ComunicacionProvider {

  url: string;
  private config = new config();
  arrayConsulta: any[] = [];
  constructor(public http: HttpClient, private _util: UtilProvider) {
    console.log('Hello ComunicacionProvider Provider');
    this.url = this.config.UrlRoot;
  }

  postIngreso(data: any) {
    const headers = this._util.getHeaders();
    let url = this.url + 'ingreso/crear';
    return this.http.post(url, data, { headers: headers });
  }

  getConsultaPersonal(data: any) {
    const headers = this._util.getHeaders();
    let url = this.url + 'ingreso/consultaPersonal';
    return this.http.post(url, data, { headers: headers });
  }
  
  getConsultaIngreso(data: any) {
    const headers = this._util.getHeaders();
    let url = this.url + 'ingreso/consulta';
    this.http.post(url, data, { headers: headers }).subscribe((res:any)=>{
      this.arrayConsulta = res.respuesta;
    },(e)=>{
      console.log(e.error);
      let msg = '';
      let status = '';
      if (e.status === 0) {
        msg = 'No hay conexión a internet';
      } else {
        msg = e.error.message;
        status = e.error.status;
      }
      this._util.confirmarMensaje(status, msg);
    });
  }

  getConsultaIngresoInfinity(data: any) {
    const headers = this._util.getHeaders();
    let url = this.url + 'ingreso/consulta';
    return this.http.post(url, data, { headers: headers });
  }
  
}
