
import { Injectable } from '@angular/core';

import{ ScanData } from "../../models/scan-data.model";
import { InAppBrowser } from '@ionic-native/in-app-browser';

import {ModalController} from "ionic-angular";
import {MapasPage} from "../../pages/mapas/mapas";
import { ComunicacionProvider } from '../comunicacion/comunicacion';
import { UsuarioProvider } from '../usuario/usuario';
/*
  Generated class for the HistorialProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HistorialService {

  private _historial:ScanData[]=[];
  usuario: any = {};

  constructor(private iab: InAppBrowser,
              private modalCtrl:ModalController, private _comu:ComunicacionProvider, private _user: UsuarioProvider) {
                this._user.cargarUsuario().then((res: any) => {
                  this.usuario = res;
                  // this.cargarHistorial();
                }).catch((err) => {
                  console.log(err);
                  this.usuario = {};
                });
                
    
  }

  // cargarHistorial(){
  //   debugger
  //   this._comu.getConsultaIngreso({id_usuario: this.usuario.id}).subscribe((res)=>{
  //     console.log(res);
  //   },(err)=>{
  //     console.log(err);
  //   });
  // }

  cargar_hidtorial(){
    return this._historial;
  }

  agregar_historial(text:string){
    let data = new ScanData( text );
    this._historial.unshift(data);
    console.log(this._historial);
    this.abrir_scan(0);
  }

  abrir_scan(index:number){
    let scanData=this._historial[index];
    console.log(scanData);
    switch(scanData.tipo){
      case "http":
        this.iab.create(scanData.info,"_system");
      break;
      case "mapa":
        this.modalCtrl.create(MapasPage, {coords: scanData.info}).present();
      break;
      default:
      console.error("Tipo no soportado");
    }
  }

}
