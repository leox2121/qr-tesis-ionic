// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { config } from '../../config';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { Platform, NavController, App, AlertController } from "ionic-angular";
import { Storage } from "@ionic/storage";

import { TabsPage, LoginPage, Tabs2Page } from "../../pages/index.paginas";
import { UsuarioProvider } from '../usuario/usuario';
import { UtilProvider } from '../util/util';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  private config = new config();
  url: string;
  urlRol: string;
  urlRegistar: string;
  urlRolSimple: string;

  constructor(public http: Http,
    private alertCtrl: AlertController,
    private _user: UsuarioProvider,
    private platform: Platform,
    private storage: Storage,
    private _util: UtilProvider,
    private app: App) {
    console.log('Hello AuthProvider Provider');
    this.url = this.config.UrlRoot + "Authenticate";
    this.urlRol = this.config.UrlRoot + "consultaRol";
    this.urlRegistar = this.config.UrlRoot + "Registrar";
    this.urlRolSimple = this.config.UrlRoot + "rol";
  }

  getRolUsuario(data: any) {
    let headers = new Headers();
    return this.http.post(this.urlRol, data, { headers: headers });
  }

  getRol() {
    let headers = new Headers();
    return this.http.get(this.urlRolSimple,  { headers: headers });
  }

  IniciarSesion(user: any, contex) {
    contex.loader.present();
    let headers = new Headers();
    this.http.post(this.url, user, { headers: headers }).
      subscribe((resp: any) => {
        let res = resp.json();
        if (res.error) {
          contex.loader.dismiss();
          this._util.Toastmensaje(res.error);
        } else {
          // debugger
          this.guardarStorage(res);
          contex.loader.dismiss();
          this._util.Toastmensaje(res.usuario.mensaje)
          let rol = res.usuario.rol;
          if(rol === '0003'){
            contex.navCtrl.setRoot(Tabs2Page);
          } else {
            contex.navCtrl.setRoot(TabsPage);
          }
        }
      }, (err) => {
        contex.loader.dismiss();
        let mensajetoast = 'No se encuentra la conexión con el servidor.';
        this._util.Toastmensaje(mensajetoast);
      });
  }

  Registrar(user: any) {
    // contex.loader.present();
    let headers = new Headers();
    return this.http.post(this.urlRegistar, user, { headers: headers });
      // subscribe((resp: any) => {
      //   let res = resp.json();
      //   if (res.error) {
      //     contex.loader.dismiss();
      //     this._util.Toastmensaje(res.error);
      //   } else {
      //     contex.navCtrl.pop();
      //     // debugger
      //     // this.guardarStorage(res);
      //     contex.loader.dismiss();
      //     this._util.Toastmensaje(res.usuario.mensaje)
      //     // let rol = res.usuario.rol;
      //     // if(rol === '0003'){
      //     //   contex.navCtrl.setRoot(Tabs2Page);
      //     // } else {
      //     //   contex.navCtrl.setRoot(TabsPage);
      //     // }
      //   }
      // }, (err) => {
      //   contex.loader.dismiss();
      //   let mensajetoast = 'No se encuentra la conexión con el servidor.';
      //   this._util.Toastmensaje(mensajetoast);
      // });
  }

  guardarStorage(data: any) {
    if (this.platform.is("cordova")) {
      this.storage.set('authenticate', data.Authenticate);
      this.storage.set('token', data.Csrf_Token);
      this.storage.set('id_usuario', data.usuario.id);
      this.storage.set('cedula', data.usuario.cedula);
      this.storage.set('rol', data.usuario.rol);
      this.storage.set('usuario', JSON.stringify(data.usuario));
      console.log("se guardo en el storage");
    } else {
      localStorage.setItem('authenticate', data.Authenticate);
      localStorage.setItem('token', data.Csrf_Token);
      localStorage.setItem('id_usuario', data.usuario.id);
      localStorage.setItem('cedula', data.usuario.cedula);
      localStorage.setItem('rol', data.usuario.rol);
      localStorage.setItem('usuario', JSON.stringify(data.usuario));
      console.log("se guardo en el storage");
    }
  }

  cerrarSesion() {
    if (this.platform.is("cordova")) {
      this.storage.clear();
    } else {
      localStorage.clear();
    }
  }

  salir() {
    this.alertCtrl.create({
      title: "Cerrar sesión",
      subTitle: "Desea salir de QrAppMed móvil",
      buttons: [
        {
          text: 'Si',
          handler: data => {
            this.cerrarSesion();
            this.app.getRootNav().setRoot(LoginPage);
          }
        },
        {
          text: 'Cancelar',
          handler: data => {
            console.log('cancelo la salida');
          }
        }
      ]
    }).present();
  }

  cargarStorage() {
    if (this.platform.is("cordova")) {
      this.storage.get("token").then(token => {
        if (token) {
          console.log('iniciando app token' + token);
          console.log("existe token");
          return true;
          //this.rootPage = TabsPage;
        } else {
          return false;
          //this.rootPage = LoginPage;
        }
      });
    } else {

      if (localStorage.getItem("token")) {
        return true;
        //this.rootPage = TabsPage;
      } else {
        return false;
        //this.rootPage = LoginPage;
      }
    }
  }

}
