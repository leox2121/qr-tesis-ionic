import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage, TabsPage, MapasPage, GuardadosPage, LoginPage, MiqrPage, Tabs2Page, QrpersonalPage, RegisterPage } from '../pages/index.paginas';
//pluggins
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { HistorialService } from '../providers/historial/historial';
import { InAppBrowser } from '@ionic-native/in-app-browser';

//servicios 
import { AgmCoreModule } from '@agm/core';
import { AuthProvider } from '../providers/auth/auth';

import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from "@ionic/storage";
import { QRCodeModule } from 'angular2-qrcode';
import { UsuarioProvider } from '../providers/usuario/usuario';
import { ComunicacionProvider } from '../providers/comunicacion/comunicacion';
import { UtilProvider } from '../providers/util/util';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    MapasPage,
    GuardadosPage,
    LoginPage,
    MiqrPage,
    Tabs2Page,
    QrpersonalPage,
    RegisterPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    QRCodeModule,
    IonicStorageModule.forRoot(),
    // IonicModule.forRoot(MyApp),
    IonicModule.forRoot(MyApp,{
      mode:'ios',
      backButtonText: 'Atras',
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDRcsURKePswx4aystpV4KcI0z_hIL1FBs'
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    MapasPage,
    GuardadosPage,
    LoginPage,
    MiqrPage,
    Tabs2Page, 
    QrpersonalPage,
    RegisterPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    HistorialService,
    InAppBrowser,
    AuthProvider,
    UsuarioProvider,
    ComunicacionProvider,
    UtilProvider
  ]
})
export class AppModule { }
