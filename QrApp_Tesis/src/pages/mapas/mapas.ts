import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-mapas',
  templateUrl: 'mapas.html',
})
export class MapasPage {

  lat: number;
  lng: number;

  constructor(private viewCtrl:ViewController, 
              public navCtrl: NavController, 
              public navParams: NavParams) {
    //this.lat = 51.678418;
    //this.lng = 7.809007;
    let coordenadas = this.navParams.get("coords").split(",");
    this.lat = Number(coordenadas[0].replace("geo:",""));
    this.lng = Number(coordenadas[1]);
    console.log(this.navParams.get("coords"));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapasPage');
  }

  cerrar_modal(){
    this.viewCtrl.dismiss();
  }

}
