import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform, AlertController, ToastController } from "ionic-angular";
import { Storage } from "@ionic/storage";

/*
  Generated class for the UtilProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UtilProvider {

  token: string;
  authenticate: string;

  constructor(public http: HttpClient, private platform: Platform, private alertCtrl: AlertController, public toastCtrl: ToastController,
    private storage: Storage) {
    this.getHeadersStorage();
    console.log('Hello UtilProvider Provider');
  }

  getHeaders() {
    this.getHeadersStorage();
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      '_token': this.token,
      "Authorization": "Bearer " + this.authenticate,
    });
    return headers;
  }

  getHeadersStorage() {
    return new Promise((resolve, reject) => {
      if (this.platform.is("cordova")) {
        this.storage.ready().then(() => {
          this.storage.get("token").then(token => {
            this.token = token;
          });
          this.storage.get("authenticate").then(authenticate => {
            this.authenticate = authenticate;
            resolve();
          });
        });
      } else {
        this.token = localStorage.getItem('token')
        this.authenticate = localStorage.getItem('authenticate')
        resolve();
      }
    });
  }

  confirmarSincrono(params, cb) {
    let alert = this.alertCtrl.create({
      title: params.titulo,
      message: params.mensaje,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Se cancelo la peticion');
          }
        },
        {
          text: 'Si estoy seguro',
          handler: () => {
            console.log('Buy clicked');
            debugger
            cb().subscribe((res) => {
              this.confirmarMensaje(`${params.titulo}`, `Haz realizado la accion ${params.accion} correctamente`)
            }, (e) => {
              console.log(e.error);
              let msg = '';
              let status = '';
              if (e.status === 0) {
                msg = 'No hay conexión a internet';
              } else {
                msg = e.error.message;
                status = e.error.status;
              }
              // if (e.status === 500) {
              //   msg = e.error.message;
              //   status = e.error.status;
              // }
              this.confirmarMensaje(status, msg);
            });
          }
        }
      ]
    });
    alert.present();
  }

  confirmarSincronoPromesa(params) {
    return new Promise((resolve, reject) => {
      let alert = this.alertCtrl.create({
        title: params.titulo,//'Guardar',
        message: params.mensaje,//'Esta seguro que desea guardar los siguientes datos?',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              console.log('Se cancelo la peticion');
            }
          },
          {
            text: 'Si estoy seguro',
            handler: () => {
              // console.log('Buy clicked');
              // debugger
              params.cb.subscribe((res) => {
                this.confirmarMensaje(`${params.titulo}`, `Haz realizado la accion ${params.accion} correctamente`)
                resolve(res);
              }, (e) => {
                debugger
                console.log(e.error);
                let msg = '';
                let status = '';
                if (e.status === 0) {
                  msg = 'No hay conexión a internet';
                } else {
                  msg = e.error.message;
                  status = e.error.status;
                }
                // if (e.status === 500) {
                //   msg = e.error.message;
                //   status = e.error.status;
                // }
                this.confirmarMensaje(status, msg);
                reject(e);
              });
            }
          }
        ]
      });
      alert.present();
    });
  }

  confirmarSincronoPromesaAuth(params) {
    return new Promise((resolve, reject) => {
      let alert = this.alertCtrl.create({
        title: params.titulo,//'Guardar',
        message: params.mensaje,//'Esta seguro que desea guardar los siguientes datos?',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              console.log('Se cancelo la peticion');
            }
          },
          {
            text: 'Si estoy seguro',
            handler: () => {
              // console.log('Buy clicked');
              // debugger
              params.cb.subscribe((res) => {
                this.confirmarMensaje(`${params.titulo}`, `Haz realizado la accion ${params.accion} correctamente`)
                resolve(res);
              }, (ex) => {
                // debugger
                console.log(ex.error);
                let e = ex.json();
                let msg = '';
                let status = '';
                if (e.status === 0) {
                  msg = 'No hay conexión a internet';
                } else {
                  msg = e.message;
                  status = e.status;
                }
                // if (e.status === 500) {
                //   msg = e.error.message;
                //   status = e.error.status;
                // }
                this.confirmarMensaje(status, msg);
                reject(e);
              });
            }
          }
        ]
      });
      alert.present();
    });
  }

  confirmarMensaje(titulo, mensaje, cssClass?) {
    const alert = this.alertCtrl.create({
      title: titulo,
      subTitle: mensaje,
      buttons: ['OK'],
      cssClass: cssClass
    });
    alert.present();
  }

  Toastmensaje(msn: string) {
    const toast = this.toastCtrl.create({
      message: msn,
      duration: 1500
    });
    toast.present();
  }


}
