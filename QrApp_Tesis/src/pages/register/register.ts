import { Component } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { UtilProvider } from '../../providers/util/util';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  formulario: FormGroup;
  cmbRoles:any[] = [];

  constructor(public fb: FormBuilder, private _auth:AuthProvider,private _util: UtilProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.formulario = this.fb.group({
      name: ['', [Validators.required]],
      apellidos: ['', [Validators.required]],
      cedula: ['', [Validators.required]],
      edad: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      // rol: ['',[Validators.required]],
    });
    // this.getRol();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  registrar() {
    let data = this.formulario.value;
    this._util.confirmarSincronoPromesaAuth({
      titulo: `Registrar Datos`, 
      mensaje: 'Esta seguro que desea guardar los siguientes datos', 
      accion: 'guardar',
      cb: this._auth.Registrar(data)
    }).then((res)=>{
      this.navCtrl.pop();
      // this._util.confirmarMensaje('','');
      // this._com.getConsultaIngreso({id_usuario: this.usuario.id});
    }).catch((err)=>{
      console.log(err);
    });
  }

  // getRol() {
  //   // this.mostrar = false;
  //   // this.formulario.get('rol').setValue('');
  //   // let data = { email: this.formulario.get('email').value };
  //   this._auth.getRol().subscribe((res: any) => {
  //     let resp = res.json();
  //     this.cmbRoles = resp.respuesta;
  //     if(this.cmbRoles.length < 1){
  //       this._util.confirmarMensaje('Error','No se encontraron roles para este usuario');
  //     }
  //   }, (err) => {
  //     console.log(err);
  //     this._util.Toastmensaje("Error al consultar roles");
  //     this.cmbRoles = [];
  //     // this.mostrar = false;
  //   });
  // }
}
