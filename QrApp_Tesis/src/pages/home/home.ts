import { Component } from '@angular/core';
//componoentes
import { 
  // ToastController, 
  Platform, 
  // AlertController, 
  App } from "ionic-angular";
//pluggins 
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
//servicios 
// import { HistorialService } from "../../providers/historial/historial";
import { AuthProvider } from '../../providers/auth/auth';
// import { LoginPage } from '../index.paginas';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { TabsPage } from '../tabs/tabs';
import { ComunicacionProvider } from '../../providers/comunicacion/comunicacion';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UtilProvider } from '../../providers/util/util';
import { Tabs2Page } from '../tabs2/tabs2';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  color: string;
  usuario: any = {};
  formulario: FormGroup;
  validation: boolean = false;

  constructor(private barcodeScanner: BarcodeScanner,
    public fb: FormBuilder,
    // public toastCtrl: ToastController,
    // public alertCtrl: AlertController,
    // private _historialService: HistorialService,
    private platform: Platform,
    private _auth: AuthProvider,
    private _user: UsuarioProvider,
    private _com: ComunicacionProvider,
    public app: App,
    public _util: UtilProvider
    ) {
    this.formulario = this.fb.group({
      id_usuario: ['', [Validators.required]],
      id_area: [''],
      opcion: ['', [Validators.required]],
    });
    this.cargarUsuario();
  }

  cargarUsuario() {
    this._user.cargarUsuario().then((res: any) => {
      this.usuario = res;
      this.formulario.get('id_usuario').setValue(res.id);
    }).catch((err) => {
      console.log(err);
      this.usuario = null;
    });
  }

  scan() {
    console.log("Realizando Scan...");
    if (!this.platform.is('cordova')) {
      console.log('solo se puede usar en dispositivos');
      this.formulario.get('id_area').setValue('2');
      this.postIngreso();
      return;
      //this._historialService.agregar_historial("http://google.com");
      // this._historialService.agregar_historial("geo:51.678418,7.809007");
      // return;
    }

    this.barcodeScanner.scan().then(barcodeData => {
      let cadena = 'Barcode data ' + barcodeData;
      console.log(cadena);
      console.log("Result: ", barcodeData.text);
      console.log("Format: ", barcodeData.format);
      console.log("Cancelled: ", barcodeData.cancelled);
      if (barcodeData.cancelled === true) {
        if(this.usuario.rol === '0003'){
          this.app.getRootNav().setRoot(Tabs2Page, { tabIndex: 1 });
        } else {
          this.app.getRootNav().setRoot(TabsPage, { tabIndex: 1 });
        }
      }
      if (barcodeData.cancelled == false && barcodeData.text != null) {
        // this._historialService.agregar_historial(barcodeData.text);
        this.formulario.get('id_area').setValue(barcodeData.text);
        this.postIngreso();
      }
    }).catch(err => {
      let cadena = 'Error ' + err;
      console.log(cadena);
      this._util.Toastmensaje(cadena);
    });
  }

  cambioColor(color: string) {
    switch (color) {
      case 'entrada':
        this.color = 'primary';
        this.formulario.get('opcion').setValue('1');
        break;
      case 'salida':
        this.color = 'danger';
        this.formulario.get('opcion').setValue('0');
        break;
      default:
        break;
    }
    this.cargarValidacion();
  }

  cargarValidacion() {
    this.validation = false; 
    let value = this.formulario.get('opcion').value;
    if(value != null  && value != '' && value != undefined){
      this.validation = true; 
    }
  }
 
  postIngreso() {
    let data = this.formulario.value;
    this._util.confirmarSincronoPromesa({
      titulo: `Guardar Marcacion de ${ data.opcion == 1 ? 'entrada' : 'salida' }`, 
      mensaje: 'Esta seguro que desa guardar los siguientes datos', 
      accion: 'guardar',
      cb: this._com.postIngreso(data)
    }).then((res)=>{
      this._com.getConsultaIngreso({id_usuario: this.usuario.id});
    }).catch((err)=>{
      console.log(err);
    });
  }

}
