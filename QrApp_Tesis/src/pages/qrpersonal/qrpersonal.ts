import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, App } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { TabsPage } from '../tabs/tabs';
import { Tabs2Page } from '../tabs2/tabs2';
import { UtilProvider } from '../../providers/util/util';
import { ComunicacionProvider } from '../../providers/comunicacion/comunicacion';
import { resUserModel } from '../../models/resUserModel';
import { empty } from 'rxjs/Observer';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

/**
 * Generated class for the QrpersonalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-qrpersonal',
  templateUrl: 'qrpersonal.html',
})
export class QrpersonalPage {

  respuesta: resUserModel;
  cedula: string;
  mostrar: boolean = false;
  mensaje: string;
  img: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private _auth: AuthProvider, private platform: Platform, private _com: ComunicacionProvider,
    private barcodeScanner: BarcodeScanner, public app: App, private _util: UtilProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QrpersonalPage');
  }

  scan() {
    // debugger
    console.log("Realizando Scan...");
    if (!this.platform.is('cordova')) {
      console.log('solo se puede usar en dispositivos');
      this.cedula = '1331016275'
      this.consultaPersonal();
      return;
    }
    this.barcodeScanner.scan().then(barcodeData => {
      let cadena = 'Barcode data ' + barcodeData;
      console.log(cadena);
      console.log("Result: ", barcodeData.text);
      console.log("Format: ", barcodeData.format);
      console.log("Cancelled: ", barcodeData.cancelled);
      if (barcodeData.cancelled === true) {
        this.app.getRootNav().setRoot(Tabs2Page, { tabIndex: 1 });
      }
      if (barcodeData.cancelled == false && barcodeData.text != null) {
        this.cedula = barcodeData.text;
        this.consultaPersonal();
      }
    }).catch(err => {
      let cadena = 'Error ' + err;
      console.log(cadena);
      this._util.Toastmensaje(cadena);
    });
  }

  consultaPersonal() {
    let ruta= '../../assets/imgs/';
    this._com.getConsultaPersonal({ cedula: this.cedula }).subscribe((res: any) => {
      this.respuesta = res.respuesta;
      this.mostrar = true;
      switch (this.respuesta.estado) {
        case true:
          this.mensaje = 'SI PUEDE PASAR A LA EMPRESA'
          this.img = ruta +'entrar.jpg';
          break;
        case false:
          this.mensaje = 'NO PUEDE PASAR A LA EMPRESA'
          this.img = ruta +'noentrar.png';
          break;
        default:
          break;
      }
    }, (err) => {
      this.mostrar = false;
      this._util.Toastmensaje(err);
    });
  }

  limpiar() {
    this.cedula = '';
    this.respuesta = null;
    this.mostrar = false;
    this.mensaje = '';
    this.img = '';
  }

}
