import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { Tabs2Page } from '../pages/tabs2/tabs2';
import { LoginPage } from '../pages/index.paginas';
import { Storage} from "@ionic/storage";
import { AuthProvider } from '../providers/auth/auth';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;
 
  constructor(private _auth: AuthProvider, private storage:Storage, private platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen) {
      // let bandera = this._auth.cargarStorage();
      // if(bandera){ this.rootPage = TabsPage } else { this.rootPage = LoginPage }
      this.cargarStorage();
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // statusBar.styleDefault();
      statusBar.backgroundColorByHexString('#3C873A');
      splashScreen.hide();
    });
  }

  cargarStorage(){
    console.log("preguntando cordova");
      if(this.platform.is("cordova")){
        console.log("cargando token");
        //this.storage.ready().then(()=>{ 

          this.storage.get("token").then( token =>{
                                  if(token){
                                    console.log('iniciando app token'+token);
                                    console.log("existe token");
                                    this.storage.get("rol").then( rol =>{
                                      if(rol === '0003'){
                                        this.rootPage = Tabs2Page;
                                      } else {
                                        this.rootPage = TabsPage;
                                      }
                                    });
                                    //this.menuCtrl.swipeEnable(true);
                                  }else{
                                    console.log("no existe token");
                                    this.rootPage = LoginPage;
                                   // this.menuCtrl.swipeEnable(false);
                                  }          
                    });
            //})
      }else{
        debugger
          if(localStorage.getItem("token")){
            let rol = localStorage.getItem("rol");
            if(rol === '0003'){
              this.rootPage = Tabs2Page;
            } else {
              this.rootPage = TabsPage;
            }
            // this.rootPage = Tabs2Page;
            //this.menuCtrl.swipeEnable(true);
          }else{
            //this.menuCtrl.swipeEnable(false);
            console.log('entro al logion y menu desactivado por el falso');
            this.rootPage = LoginPage;
            
          }    
      }
  }

}

