import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QrpersonalPage } from './qrpersonal';

@NgModule({
  declarations: [
    QrpersonalPage,
  ],
  imports: [
    IonicPageModule.forChild(QrpersonalPage),
  ],
})
export class QrpersonalPageModule {}
