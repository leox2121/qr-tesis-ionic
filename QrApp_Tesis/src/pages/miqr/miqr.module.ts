import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MiqrPage } from './miqr';

@NgModule({
  declarations: [
    MiqrPage,
  ],
  imports: [
    IonicPageModule.forChild(MiqrPage),
  ],
})
export class MiqrPageModule {}
